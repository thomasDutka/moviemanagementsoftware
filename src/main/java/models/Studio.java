package models;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "studio")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "studio")
public class Studio {
    @Id
    @GeneratedValue
    @Column(name = "studio_id")
    private int studioID;
    @XmlAttribute(name = "name")
    @Column(name = "studio_name")
    private String name;
    @XmlAttribute(name = "countrycode")
    @Column(name = "studio_countrycode")
    private String countryCode;
    @XmlAttribute(name = "postcode")
    @Column(name = "studio_postcode")
    private String postCode;
    @XmlTransient
    @OneToMany(mappedBy = "movie_studio")
    private List<Movie> movies;

    public Studio() {
    }

    public Studio(String name, String countryCode, String postCode) {
        this.name = name;
        this.countryCode = countryCode;
        this.postCode = postCode;
    }

    public Studio(String name, String countryCode, String postCode, List<Movie> movies) {
        this.name = name;
        this.countryCode = countryCode;
        this.postCode = postCode;
        this.movies = movies;
    }

    public int getStudioID() {
        return studioID;
    }

    public void setStudioID(int studioID) {
        this.studioID = studioID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovies(List<Movie> movies) {
        this.movies.addAll(movies);
    }
}
