package models;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "actor")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "actor")
public class Actor implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "actor_id")
    private int actorID;
    @XmlAttribute(name = "firstname")
    @Column(name = "actor_firstname")
    private String firstname;
    @XmlAttribute(name = "lastname")
    @Column(name = "actor_lastname")
    private String lastname;
    @XmlAttribute(name = "sex")
    @Column(name = "actor_sex")
    private String sex;
    @XmlAttribute(name = "birthday")
    @Column(name = "actor_birthday")
    private Date birthday;
    @XmlTransient
    @ManyToMany
    @JoinColumn(name = "actor_movies")
    private List<Movie> movies;

    public Actor() {
    }

    public Actor(String firstname, String lastname, String sex, Date birthday, List<Movie> movies) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.sex = sex;
        this.birthday = birthday;
        this.movies = movies;
    }

    public int getActorID() {
        return actorID;
    }

    public void setActorID(int actorID) {
        this.actorID = actorID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
