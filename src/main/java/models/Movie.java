package models;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "movie")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "movie")
public class Movie implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "movie_id")
    private int movieID;
    @XmlAttribute(name = "title")
    @Column(name = "movie_title")
    private String title;
    @XmlAttribute(name = "description")
    @Column(name = "movie_description")
    private String description;
    @XmlAttribute(name = "length")
    @Column(name = "movie_length")
    private int length;
    @XmlAttribute(name = "releaseyear")
    @Column(name = "movie_releasyear")
    private int releaseYear;
    @XmlAttribute(name = "genre")
    @Column(name = "movie_genre")
    private String genre;
    @XmlElementWrapper(name = "actors")
    @XmlElement(name = "actor")
    @ManyToMany(mappedBy = "actor")
    private List<Actor> actors;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "movie_studio")
    @XmlElement(name = "studio")
    private Studio studio;

    public Movie() {
    }

    public Movie(String title, String description, int length, int releaseYear, String genre, List<Actor> actors, Studio studio) {
        this.title = title;
        this.description = description;
        this.length = length;
        this.releaseYear = releaseYear;
        this.genre = genre;
        this.actors = actors;
        this.studio = studio;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public Studio getStudio() {
        return studio;
    }

    public void setStudio(Studio studio) {
        this.studio = studio;
    }
}
